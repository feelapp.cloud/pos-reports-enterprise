odoo.define('aspl_pos_report_ee.models', function (require) {
    "use strict";

    var models = require('point_of_sale.models');
    var DB = require('point_of_sale.DB');
    var rpc = require('web.rpc');
    var Printer = require('point_of_sale.Printer').Printer;
    var SuperPosModel = models.PosModel;

    models.PosModel = models.PosModel.extend({
        initialize: function(session, attributes) {
            this.all_pos_session = [];
            this.all_locations = [];
            SuperPosModel.prototype.initialize.call(this, session, attributes);
        },
        load_server_data: function(){
            var self = this;
            return SuperPosModel.prototype.load_server_data.call(this, arguments).then(function () {
                var session_params = {
                    model: 'pos.session',
                    method: 'search_read',
                    domain: [['state','=','opened']],
                    fields: ['id','name','config_id'],
                    orderBy: [{ name: 'id', asc: true}],
                }
                rpc.query(session_params, {async: false})
                .then(function(sessions){
                    if(sessions && sessions[0]){
                        self.all_pos_session = sessions;
                    }
                });
                var stock_location_params = {
                    model: 'stock.location',
                    method: 'search_read',
                    domain: [['usage','=','internal'],['company_id','=',self.company.id]],
                    fields: ['id','name','company_id','complete_name'],
                }
                rpc.query(stock_location_params, {async: false})
                .then(function(locations){
                    if(locations && locations[0]){
                        self.all_locations = locations;
                    }
                });
                var inventory_params = {
                    model: 'pos.order',
                    method: 'load_inventory_setting',
                }
                rpc.query(inventory_params)
                .then(function(parameter){
                    if(parameter && parameter[0]){
                        if(parameter[0].point_of_sale_update_stock_quantities == 'closing'){
                            self.inventory_location = true;
                        }
                    }
                }).catch(function(){
                    console.log("Connection lost");
                });
            });
        },
     });

    var _super_order = models.Order.prototype;
    models.Order = models.Order.extend({
        get_number_of_print : function(){
            return this.number_of_print;
        },
        set_number_of_print : function(number){
            this.number_of_print = number;
        },
        set_order_summary_report_mode: function(order_summary_report_mode) {
            this.order_summary_report_mode = order_summary_report_mode;
        },
        set_number_of_print : function(number){
            this.number_of_print = number;
        },
    });

});
