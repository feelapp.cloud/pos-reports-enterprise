odoo.define('aspl_pos_report_ee.PaymentSummaryButton', function(require) {
    'use strict';

    const PosComponent = require('point_of_sale.PosComponent');
    const ProductScreen = require('point_of_sale.ProductScreen');
    const { useListener } = require('web.custom_hooks');
    const Registries = require('point_of_sale.Registries');
    var rpc = require('web.rpc');

    class PaymentSummaryButton extends PosComponent {
        constructor() {
            super(...arguments);
            useListener('click', this.onClick);
        }
        async getPaymentReportData(val){
            return this.rpc({
                model: 'pos.order',
                method: 'payment_summary_report',
                args: [val]
            });
        }
        async onClick() {
            const { confirmed, payload } = await this.showPopup('PaymentSummaryPopup', {
                title: this.env._t('Payment Summary'),
            });

            if (confirmed) {
                var self = this;
                var order = self.env.pos.get_order();
                var val = {};

                const { StartDate, EndDate, CurrentSession, PaymentSelectData } = payload;

                if(CurrentSession){
                    Object.assign(val, { summary: PaymentSelectData,
                                    session_id:  this.env.pos.pos_session.id });
                    self.env.pos.payment_print_date = false;
                } else {
                    Object.assign(val, { start_date: StartDate, end_date: EndDate,
                                                  summary: PaymentSelectData });
                    self.env.pos.payment_print_date = true;
                    self.env.pos.payment_date = [ payload['StartDate'], payload['EndDate'] ];
                }
                if (val && !$.isEmptyObject(val)) {
                    let payment_result = await this.getPaymentReportData(val);
                    const {JournalData, SalesMenData, TotalSummary } = payment_result;
                    if($.isEmptyObject(JournalData) && $.isEmptyObject(SalesMenData)){
                        alert("No records found!");
                        return;
                    }
                    if (this.env.pos.config.iface_print_via_proxy) {
                        const report = this.env.qweb.renderToString('PaymentSummaryReceipt',{
                            props: {
                                'CompanyData': order.getOrderReceiptEnv().receipt,
                                'JournalReportData': !$.isEmptyObject(JournalData) ? JournalData : false,
                                'SalesReportData': !$.isEmptyObject(SalesMenData) ? SalesMenData : false,
                                'TotalSummaryData': TotalSummary,
                            }, env : this.env
                        });
                        const rec = Array(1).fill(report)
                        const printResult = await this.env.pos.proxy.printer.print_receipt(rec);
                        if (!printResult.successful) {
                            await this.showPopup('ErrorPopup', {
                                title: printResult.message.title,
                                body: printResult.message.body,
                            });
                        }
                    } else{
                        self.showScreen('ReceiptScreen', {'check':'from_payment_summary',
                            'receipt': order.getOrderReceiptEnv().receipt,
                            'journal_details': !$.isEmptyObject(JournalData) ? JournalData : false,
                            'salesmen_details': !$.isEmptyObject(SalesMenData) ? SalesMenData : false,
                            'total_summary': TotalSummary,
                        });
                    }
                }
            }
        }
    }
    PaymentSummaryButton.template = 'PaymentSummaryButton';

    ProductScreen.addControlButton({
        component: PaymentSummaryButton,
        condition: function() {
            return this.env.pos.config.payment_summary;
        },
    });

    Registries.Component.add(PaymentSummaryButton);

    return PaymentSummaryButton;

});
